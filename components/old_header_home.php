<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=DM Sans">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <title>Document</title>
</head>
<body>

<!-- Header Code Starting -->

<nav class="navbar navbar-light search-navbar">
  <div class="container">
    <a class="navbar-brand" href="index.php">
        <div class="main-logo-div">
                <img src="assets/images/logo-1x.png" />
        </div>
    </a>
    <button class="navbar-toggler outer-boundary" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
        <span class="navbar-toggler-icon">
            <svg xmlns="http://www.w3.org/2000/svg" style="color: #1660D6;" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
            </svg>
        </span>
    </button>
    <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
      <div class="offcanvas-header d-flex justify-content-end">
        <!-- <h5 class="offcanvas-title" id="offcanvasNavbarLabel">Offcanvas</h5> -->
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
      </div>
      <div class="offcanvas-body">
            <div class="d-flex align-items-center flex-column justify-content-center header-right-div">
                        <button type="button" class="btn btn-primary log-in-btn w-100">Log in</button>
                        <a href="#" class="w-100 d-flex justify-content-center align-items-center upload-resume-btn">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12" />
                            </svg>
                            <p class="mb-0">Upload resume</p>
                        </a>
                        <a href="#" class="header-blog">Blog</a>
            </div>        
      </div>
    </div>
  </div>
</nav>



<header class="display-none">
    <div class="container">
        <div class="d-flex align-items-center main-header-div">
        <a href="index.php">
            <div class="main-logo-div">
                <img src="assets/images/logo-1x.png" />
            </div>
        </a>
            <div class="d-flex align-items-center header-right-div">
                <a href="#" class="header-blog">Blog</a>
                <button type="button" class="btn btn-primary log-in-btn">Log in</button>
                <a href="#" class="d-flex justify-content-center align-items-center upload-resume-btn">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12" />
                    </svg>
                    <p class="mb-0">Upload resume</p>
                </a>
            </div>
        </div>
    </div>
</header>
<!-- Header Code Ending -->