<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=DM Sans">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../assets/css/responsive.css">
    <title>Document</title>
</head>
<body>

<!-- Header Code Starting -->

<nav class="navbar navbar-light search-navbar">
  <div class="container">
    <a class="navbar-brand" href="../index.php">
        <div class="main-logo-div">
                <img src="../assets/images/logo-1x.png" />
        </div>
    </a>
    <button class="navbar-toggler outer-boundary" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
        <span class="navbar-toggler-icon">
            <svg xmlns="http://www.w3.org/2000/svg" style="color: #1660D6;" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
            </svg>
        </span>
    </button>
    <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
      <div class="offcanvas-header d-flex justify-content-end">
        <!-- <h5 class="offcanvas-title" id="offcanvasNavbarLabel">Offcanvas</h5> -->
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
      </div>
      <div class="offcanvas-body">
            <div class="d-flex align-items-center flex-column justify-content-center header-right-div">
                        <button type="button" class="btn btn-primary log-in-btn w-100">Log in</button>
                        <a href="#" class="w-100 d-flex justify-content-center align-items-center upload-resume-btn">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12" />
                            </svg>
                            <p class="mb-0">Upload resume</p>
                        </a>
                        <a href="#" class="header-blog">Blog</a>
            </div>        
      </div>
    </div>
  </div>
</nav>


<header>
    <div class="container" style="position: relative; max-height: 270px;">
        <div class="d-flex align-items-center main-header-div main-search-head-div">
            <a href="../index.php">
                <div class="main-logo-div display-none">
                <img src="../assets/images/logo-1x.png" />
                </div>
            </a>

            <div class="jobs-search-main-div d-flex jobs-search-main-div-x2">
                <div class="briefcase-div d-flex justify-content-center align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                    </svg>
                </div>
                <div class="job-input-div d-flex justify-content-center align-items-center">
                    <input type="text" placeholder="Jobs by skills, companies, designation" class="form-control seacrh-jobs-input outer-boundary" >
                </div>

                <!-- Search job button -->
                <button type="button" class="btn outer-boundary p-0" id="job-search-btn">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="#808080" style="width: 28px; ">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                    </svg>
                </button>
                <!-- Search job button -->

                <!-- Job filter button -->
                <button type="button" class="btn outer-boundary" data-bs-toggle="modal" data-bs-target="#exampleModal" id="job-filter-btn">
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="#808080" style="width: 28px;">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 6V4m0 2a2 2 0 100 4m0-4a2 2 0 110 4m-6 8a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4m6 6v10m6-2a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4" />
                    </svg>
                </button>
                <!-- Job filter button -->
            </div>

            <div class="search-bar-hr display-none"></div>

                <div class="country-main-div d-flex align-items-center display-none">
                    <div class="flag-select-div">
                        <div class="dropdown">
                            <button class="btn btn-flg outer-boundary dropdown-toggle d-flex align-items-center" type="button" id="dropdownMenu2" data-bs-toggle="dropdown" aria-expanded="false">
                            <div class="flag-showing-div"></div>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                <li><button class="dropdown-item" type="button">Action</button></li>
                                <li><button class="dropdown-item" type="button">Another action</button></li>
                                <li><button class="dropdown-item" type="button">Something else here</button></li>
                            </ul>
                        </div>
                    </div>
                    <div class="job-input-div d-flex justify-content-center align-items-center">
                        <input type="text" placeholder="Country, state, city" class="form-control seacrh-country-input outer-boundary">
                    </div>
                </div>

                <div class="find-jobs-btn-div display-none">
                    <button type="button" class="btn btn-primary find-job-btn">Find jobs</button>
                </div>

                </div>

                <div class="pages-show mt-3">
                    <h4>Showing: Page 1 of 1,105 jobs</h4>
                </div>

                <!-- Search pop-up model box code -->
                <div class=" flex-wrap flex-column" id="modal-search-div">
                    <div class="jobs-search-main-div d-flex jobs-search-main-div-x2">
                        <div class="briefcase-div d-flex justify-content-center align-items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                            </svg>
                        </div>
                        <div class="job-input-div d-flex justify-content-center align-items-center">
                            <input type="text" placeholder="Jobs by skills, companies, designation" class="form-control seacrh-jobs-input outer-boundary">
                        </div>

                        <!-- Job filter button -->
                        <button type="button" class="btn outer-boundary" data-bs-toggle="modal" data-bs-target="#exampleModal" id="job-filter-btn">
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="#808080" style="width: 28px;">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 6V4m0 2a2 2 0 100 4m0-4a2 2 0 110 4m-6 8a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4m6 6v10m6-2a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4" />
                            </svg>
                        </button>
                        <!-- Job filter button -->
                    </div>

                    <div class="search-bar-hr pop-hr-search-bar"></div>

                    <div class="country-main-div d-flex align-items-center pop-modal-flag-field">
                        <div class="flag-select-div">
                            <div class="dropdown">
                                <button class="btn btn-flg outer-boundary dropdown-toggle d-flex align-items-center" type="button" id="dropdownMenu2" data-bs-toggle="dropdown" aria-expanded="false">
                                <div class="flag-showing-div"></div>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                    <li><button class="dropdown-item" type="button">Action</button></li>
                                    <li><button class="dropdown-item" type="button">Another action</button></li>
                                    <li><button class="dropdown-item" type="button">Something else here</button></li>
                                </ul>
                            </div>
                        </div>
                        <div class="job-input-div d-flex justify-content-center align-items-center">
                            <input type="text" placeholder="Country, state, city" class="form-control seacrh-country-input outer-boundary">
                        </div>
                    </div>

                    <div class="find-jobs-btn-div w-100 mt-2">
                        <button type="button" class="btn btn-primary find-job-btn mr-0 w-100">Find jobs</button>
                    </div>

                    <div class="pt-2 w-100 mt-2">
                        <h3 class="hiding-modal-h3">Hide search</h3>
                    </div>

                </div>

                <!-- Search pop-up model box code -->

        </div>
            
            

    </div>
</header>

