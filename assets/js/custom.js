// create job alert functionality start---------------------------------------------->>>

$(document).ready(function() {
    $('#job-alrt-input').click(function() {
      if (!$(this).is(':checked')) {
        Swal.fire({
            icon: 'error',
            title: 'Jobs alerts',
            text: 'Successfully deactivated!',
            showClass: {
                popup: 'animate__animated animate__fadeInDown'
              },
              hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
              }
          })
      }

      else {
        Swal.fire({
            icon: 'success',
            title: 'Jobs alerts',
            text: 'Successfully activated!',
            showClass: {
                popup: 'animate__animated animate__fadeInDown'
              },
              hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
              }
          })
      }
    });
  });

// create job alert functionality end---------------------------------------------->>>
//
//
//
// Bookmarking job functionality Start---------------------------------------------->>>

$('.bookmark-job').click(function() {
    var getState = $(this).attr("fill");
    if (getState == "none"){
        $(this).attr("fill", "#FDC25F");
        $(this).css("color","#FDC25F");
        Swal.fire({
            icon: 'success',
            title: 'Job',
            timer:2000,
            text: 'Bookmarked Successfully!'
          })
    }
    else {
        $(this).attr("fill", "none");
        $(this).css("color","#808080");
        Swal.fire({
            icon: 'error',
            title: 'Job',
            timer:2000,
            text: 'Removed Successfully!'
          })
    }
});

// Bookmarking job functionality end---------------------------------------------->>>
//
//
//
// Search Page manual Mobile Div functionality Start---------------------------------------------->>>

if (window.matchMedia("(min-width: 1025px)").matches) {
    $('#modal-search-div').css("display","none");
}

if (window.matchMedia("(max-width: 1024px)").matches) {
$('#job-search-btn, #seacrh-jobs-input').click(function() {
    $('#modal-search-div').css("display","flex");
});

$('.hiding-modal-h3').click(function() {
    $('#modal-search-div').css("display","none");
});
}

// Search Page manual Mobile Div functionality end---------------------------------------------->>>



var Alert = /** @class */ (function () {
  //constructor 
  function Alert(type, message) {
      this.type = type;
      this.message = message;
  }
  //function 
  Alert.prototype.success = function (message) {
      this.type = "success";
      this.message = message;
      console.log(this.type + this.message);
  };
  return Alert;
}());

var alert = new Alert();
