<?php include("../components/header_search.php") ?>

<!-- Search Page Section -->
<section class="policy-section pb-0">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="faq-main-div text-center d-flex align-items-center flex-column" > 
                    <img src="../assets/images/policy.png" style="max-width:100%;"/>
                    <h3 class="mt-1">TOS and Privacy</h3>                    
                </div>
            </div>

            <div class="col-md-12 mt-4">
                <p class="faq-footer-p mb-3">Jnee (“Company” or “We”) respect your privacy and are committed to protecting it through our compliance with this policy.</p>
                <p class="faq-footer-p mb-3">This policy describes the types of information we may collect from you or that you may provide when you visit the website you linked from or Jnee (collectively the “Website”) and our practices for collecting, using, maintaining, protecting and disclosing that information.</p>
                <p class="faq-footer-p mb-1">This policy applies to information we collect:</p>
                <ul class="mb-3">
                    <li class="faq-footer-p ">On this Website.</li>
                    <li class="faq-footer-p ">In e­mail, text and other electronic messages between you and this Website.</li>
                    <li class="faq-footer-p ">When you interact with our advertising and applications on third­party websites and services, if those applications or advertising include links to this policy.</li>
                    <li class="faq-footer-p ">When you post comments on our Website through social websites, such as, but not limited to, Facebook.</li>
                </ul>
                <p class="faq-footer-p mb-1">It does not apply to information collected by:</p>
                <ul class="mb-3">
                    <li class="faq-footer-p ">Us offline or through any other means, including on any other website operated by Company or any third party (including our affiliates and subsidiaries); or</li>
                    <li class="faq-footer-p ">Any third party (including our affiliates and subsidiaries), including through any application or content (including advertising) that may link to or be accessible from or on the Website</li>
                </ul>
                <p class="faq-footer-p mb-4">Please read this policy carefully to understand our policies and practices regarding your information and how we will treat it. If you do not agree with our policies and practices, your choice is not to use our Website. By accessing or using this Website, you agree to this privacy policy. This policy may change from time to time (see Changes to our Privacy Policy). Your continued use of this Website after we make changes is deemed to be acceptance of those changes, so please check the policy periodically for updates.</p>
                <h4 class="policy-footer-h4 mt-3">Still have questions?</h4>
                <p class="faq-footer-p mb-3">Our Website is not intended for children under 13 years of age. No one under age 13 may provide any personal information to or on the Website. We do not knowingly collect personal information from children under 13. If we learn we have collected or received personal information from a child under 13 without verification of parental consent, we will delete that information. If you believe we might have any information from or about a child under 13, please contact us at the email address in the last paragraph below.</p>
                <h4 class="policy-footer-h4 mt-3">Information We Collect About You and How We Collect It</h4>
                <p class="faq-footer-p mb-1">We collect several types of information from and about users of our Website, including information:</p>
                <ul class="mb-3">
                    <li class="faq-footer-p ">by which you may be personally identified, such as your name and e­mail address, (“personal information”);</li>
                    <li class="faq-footer-p ">that is about you but individually does not identify you; and/or</li>
                    <li class="faq-footer-p ">about your internet connection, the equipment you use to access our Website and usage details.</li>
                </ul>
                <p class="faq-footer-p mb-1">We collect this information:</p>
                <ul class="mb-3">
                    <li class="faq-footer-p ">Directly from you when you provide it to us.</li>
                    <li class="faq-footer-p ">Automatically as you navigate through the site. Information collected automatically may include usage details, IP addresses and information collected through cookies, web beacons, and other tracking technologies.</li>
                    <li class="faq-footer-p ">From third parties, for example, our business partners.</li>
                </ul>
                <p class="faq-footer-p mb-3">Information You Provide to Us</p>
                <p class="faq-footer-p mb-1">The information we collect on or through our Website may include:</p>
                <ul class="mb-3">
                    <li class="faq-footer-p ">Information that you provide by filling in forms on our Website. This includes information provided at the time of registering to use our Website, subscribing to our email list, posting comments or content, or requesting further services. We may also ask you for information when you enter a contest or promotion sponsored by us, and when you report a problem with our Website.</li>
                    <li class="faq-footer-p ">Records and copies of your correspondence (including e­mail addresses), if you contact us.</li>
                </ul>
                <p class="faq-footer-p mb-3">You also may provide information or comments to be published or displayed (hereinafter, “posted”) on public areas of the Website, or transmitted to third parties (collectively, “User Contributions”). Your User Contributions are posted on and transmitted to others at your own risk.</p>
                <p class="faq-footer-p mb-0">Information We Collect through Automatic Data Collection Technologies</p>
                <p class="faq-footer-p mb-3">As you navigate through and interact with our Website, we may use automatic data collection technologies to collect certain information about your equipment, browsing actions and patterns, including:</p>
                <ul class="mb-3">
                    <li class="faq-footer-p ">Details of your visits to our Website, including traffic data, location data, logs and other communication data and the resources that you access and use on the Website.</li>
                    <li class="faq-footer-p ">Information about your computer and internet connection, including your IP address, operating system and browser type.</li>
                </ul>
                <p class="faq-footer-p mb-3">We also may use these technologies to collect information about your online activities over time and across third­party websites or other online services (behavioral tracking). We do not respond to “Do Not Track” signals or frameworks requesting an alteration to our online tracking of individual users who visit our Website.</p>
                <p class="faq-footer-p mb-1">The information we collect automatically is statistical data and does not include personal information, but we may maintain it or associate it with personal information we collect in other ways or receive from third parties. It helps us to improve our Website and to deliver a better and more personalized service, including by enabling us to:</p>
                <ul class="mb-3">
                    <li class="faq-footer-p ">Estimate our audience size and usage patterns.</li>
                    <li class="faq-footer-p ">Store information about your preferences, allowing us to customize our Website according to your individual interests.</li>
                    <li class="faq-footer-p ">Speed up your searches.</li>
                    <li class="faq-footer-p ">Recognize you when you return to our Website.</li>
                </ul>
                <h4 class="policy-footer-h4 mt-3">The technologies we use for this automatic data collection may include:</h4>
            </div>

        </div>
    </div>
</section>
<!-- Search Page Section  -->



<?php include("../components/footer.php") ?>


