<?php include("components/header_common_home.php") ?>

<!-- Banner Code Starting -->
<section class="banner-section">
    <div class="banner-text-div">
        <div class="banner-text-inner-div">
            <h2>Your <span style="color: #1660D6;">Dream Job</span> is just a search away!</h2>
        </div>
        <div class="banner-text-inner-div banner-text-inner-div2">
            <p>We know finding a job is tough, but <span style="color: #1660D6;">Jnee</span> makes it easier than ever</p>
        </div>
    </div>

<!-- Search Bar Input Field code Start -->
    <div class="banner-searchbar-div d-flex align-items-center">
        <div class="jobs-search-main-div d-flex">
            <div class="briefcase-div d-flex justify-content-center align-items-center">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                </svg>
            </div>
            <div class="job-input-div d-flex justify-content-center align-items-center">
                <input type="text" placeholder="Jobs by skills, companies, designation" class="form-control seacrh-jobs-input outer-boundary">
            </div>
        </div>

    <div class="search-bar-hr"></div>

        <div class="country-main-div d-flex align-items-center">
            <div class="flag-select-div">
                <div class="dropdown">
                    <button class="btn btn-flg outer-boundary dropdown-toggle d-flex align-items-center" type="button" id="dropdownMenu2" data-bs-toggle="dropdown" aria-expanded="false">
                    <div class="flag-showing-div"></div>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <li><button class="dropdown-item" type="button">Action</button></li>
                        <li><button class="dropdown-item" type="button">Another action</button></li>
                        <li><button class="dropdown-item" type="button">Something else here</button></li>
                    </ul>
                </div>
            </div>
            <div class="job-input-div d-flex justify-content-center align-items-center">
                <input type="text" placeholder="Country, state, city" class="form-control seacrh-country-input outer-boundary">
            </div>
        </div>

        <div class="find-jobs-btn-div">
            <a href="search/index.php" class="w-100" ><button type="button" class="btn btn-primary find-job-btn">Find jobs</button></a>
        </div>

    </div>

    <hr class="banner-botton-hr" >
<!-- Search Bar Input Field code End -->
</section>
<!-- Banner Code Ending -->

<!-- Finding Job Infomation Section Code Starting -->
<section class="job-info-section">
    <div class="container" style="max-width:1200px;">
        <div class="row">
            <div class="col-md-12 secondary-heading-div">
                <h4>Find a job using <span style="color: #1660D6;">Jnee in</span> 3 easy steps</h4>
                <p>We know finding a job is tough, but <span style="color: #1660D6;">Jnee</span> makes it easier than ever</p>
            </div>
            <div class="col-md-6">
                <div class="job-info-img-div d-flex justify-content-center align-items-center">
                    <img src="assets/images/job-img-1.png" class=""/>
                </div>
            </div>
            <div class="col-md-6 d-flex justify-content-center align-items-center">
                <div class="job-info-txt-div">
                    <h4>Search from <span style="color: #1660D6;">wherever</span> you are</h4>
                    <p>Enter the skills, jobs you want or what company you'd like to join Then in the next field put the location</p>
                    <p>You can enter for example Php, java, python and Tokyo in the location field</p>
                </div>
            </div>
            <div class="row">
            <div class="col-md-6" id="mid-div-revrsng-1">
                <div class="job-info-img-div d-flex justify-content-center align-items-center">
                    <img src="assets/images/job-img-2.png" class=""/>
                </div>
            </div>
            <div class="col-md-6 d-flex justify-content-center align-items-center" id="mid-div-revrsng-2">
                <div class="job-info-txt-div">
                    <h4>Pick & Filter from a <span style="color: #1660D6;">curated list</span> of jobs</h4>
                    <p>Jnee provides you the latest job openings according to the jobs you are interested in.</p>
                    <p>You can also tweak your requirements with powerfull filters.</p>
                </div>
            </div>
        </div>
            <div class="col-md-6">
                <div class="job-info-img-div d-flex justify-content-center align-items-center">
                    <img src="assets/images/job-img-3.png" class=""/>
                </div>
            </div>
            <div class="col-md-6 d-flex justify-content-center align-items-center">
                <div class="job-info-txt-div">
                    <h4>Go through the job details and <span style="color: #1660D6;">apply easily</span></h4>
                    <p>Now you can apply for the job which you found best fit for you</p>
                    <p>Imagine, if you can apply for the job, simply with one touch.</p>
                </div>
            </div>
        </div>
</div>
</section>
<!-- Finding Job Infomation Section Code Ending -->

<!-- Our Top Employers Section Code Starting -->
<section class="Top-Emplyrs-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 secondary-heading-div mb-4">
                <h4>Our <span style="color: #1660D6;">Top Employers</span> </h4>
            </div>
            <div class="col-md-12 d-flex justify-content-lg-between justify-content-md-center justify-content-sm-center mb-4 align-items-center flex-wrap emptyr-main-img-div">
                <img src="assets/images/part-1.png" class=""/>
                <img src="assets/images/part-2.png" class=""/>
                <img src="assets/images/part-3.png" class=""/>
                <img src="assets/images/part-4.png" class=""/>
                <img src="assets/images/part-5.png" class=""/>
                <img src="assets/images/part-6.png" class=""/>
                <img src="assets/images/part-7.png" class=""/>
            </div>
        </div>
    </div>
</section>
<!-- Our Top Employers Section Code Ending -->

<!-- Blog Section Code Starting -->
<section class="Blog-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 secondary-heading-div mb-4">
                <h4>Curated Reads for your <span style="color: #1660D6;">Job Hunt</span> </h4>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card w-100">
                    <img src="assets/images/blog-1.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title secondary-font-style">Title of the Blog</h5>
                            <p class="card-text">Desciption of the blog in 30-40 characters</p>
                            <a href="#" class="d-flex align-items-center btn outer-boundary rm-btn">Read more <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3" />
                            </svg></a>
                        </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card w-100">
                    <img src="assets/images/blog-2.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title secondary-font-style">Title of the Blog</h5>
                            <p class="card-text">Desciption of the blog in 30-40 characters</p>
                            <a href="#" class="d-flex align-items-center btn outer-boundary rm-btn">Read more <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3" />
                            </svg></a>
                        </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card w-100">
                    <img src="assets/images/blog-3.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title secondary-font-style">Title of the Blog</h5>
                            <p class="card-text">Desciption of the blog in 30-40 characters</p>
                            <a href="#" class="d-flex align-items-center btn outer-boundary rm-btn">Read more <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3" />
                            </svg></a>
                        </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="card w-100">
                    <img src="assets/images/blog-4.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title secondary-font-style">Title of the Blog</h5>
                            <p class="card-text">Desciption of the blog in 30-40 characters</p>
                            <a href="#" class="d-flex align-items-center btn outer-boundary rm-btn">Read more <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3" />
                            </svg></a>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include("components/footer_home.php") ?>





