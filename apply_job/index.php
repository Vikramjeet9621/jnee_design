<?php include("../components/header_search.php") ?>

<!-- Search Page Section -->
<section class="search-page-section">
    <div class="container">
        <div class="row">

            <div class="col-lg-12 main-jobs-container">

                <div class="common-data-div">

                    <div class="job-dsply-div mt-0 d-flex justify-content-between flex-lg-row flex-md-column ">
                        <div class="bookmrk-div-1"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8.684 13.342C8.886 12.938 9 12.482 9 12c0-.482-.114-.938-.316-1.342m0 2.684a3 3 0 110-2.684m0 2.684l6.632 3.316m-6.632-6l6.632-3.316m0 0a3 3 0 105.367-2.684 3 3 0 00-5.367 2.684zm0 9.316a3 3 0 105.368 2.684 3 3 0 00-5.368-2.684z" />
                            </svg>
                        </div>

                        <div class="bookmrk-div-1 bookmrk-div-2"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 bookmark-job" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                            </svg>
                        </div>

                        <div class="">
                            <p class="secondary-font-style">PHP Developer</p>
                            <h4 class="mb-3">IPAT Tech Solution</h4>
                            <div class="d-flex align-items-center flex-wrap mb-3">

                                <div class="d-flex align-items-center job-locations">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                                    </svg>
                                    <p class="black-primary mb-0">New york, US</p>
                                </div>

                                <div class="d-flex align-items-center job-locations">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 14v3m4-3v3m4-3v3M3 21h18M3 10h18M3 7l9-4 9 4M4 10h16v11H4V10z" />
                                    </svg>
                                    <p class="black-primary mb-0">Miles</p>
                                </div>

                                <div class="d-flex align-items-center job-locations">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                                    </svg>
                                    <p class="black-primary mb-0">2-5 Years</p>
                                </div>

                                <div class="d-flex align-items-center job-locations">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                                    </svg>
                                    <p class="black-primary mb-0">Remote</p>
                                </div>

                                <div class="d-flex align-items-center job-locations">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 8h6m-5 0a3 3 0 110 6H9l3 3m-3-6h6m6 1a9 9 0 11-18 0 9 9 0 0118 0z" />
                                    </svg>
                                    <p class="black-primary mb-0">20k-60k</p>
                                </div>

                            </div>
                            
                            <p class="job-post-text-p mb-4" >Posted 5hrs ago</p>
                        </div>

                        <div class="apply-btn-div">
                            <button type="button" class="mt-0 btn btn-primary common-btn">Apply</button>
                        </div>

                    </div>

                    <div class="job-descrptn-main-div">
                        <label class="mt-4">Our client is one of the best Fintech , Financial planner and wealth management company in India, offers comprehensive financial planning for senior citizens and wealth management solutions for golden years of life. Their mission is to help current and upcoming golden-agers live their dream life- happy second innings, free of financial worries.</label>
                        <h3>Job Responsibilities:</h3>
                        <ul class="list-group jd-ul-li">
                            <li>Develop, record and maintain cutting edge web-based PHP applications on portal plus premium service platforms</li>
                            <li>Build innovative, state-of-the-art applications and collaborate with the User Experience (UX) team</li>
                            <li>Ensure HTML, CSS, and shared JavaScript is valid and consistent across applications</li>
                            <li>Prepare and maintain all applications utilizing standard development tools</li>
                            <li>Utilize backend data services and contribute to increase existing data services API</li>
                            <li>Lead the entire web application development life cycle right from concept stage to delivery and post launch support</li>
                            <li>Convey effectively with all task progress, evaluations, suggestions, schedules along with technical and process issues</li>
                            <li>Document the development process, architecture, and standard components</li>
                            <li>Coordinate with co-developers and keeps project manager well informed of the status of development effort and serves as liaison between development staff and project manager</li>
                            <li>Keep abreast of new trends and best practices in web development</li>
                        </ul>
                        <h3>Requirements and Qualification:</h3>
                        <ul class="list-group jd-ul-li">
                            <li>Previous working experience as a PHP / Laravel developer for minimum 2 years</li>
                            <li>BS/MS degree in Computer Science, Engineering, MIS or similar relevant field</li>
                            <li>In depth knowledge of object-oriented PHP and Laravel 5 PHP Framework</li>
                            <li>Hands on experience with SQL schema design, SOLID principles, REST API design</li>
                            <li>Software testing (PHPUnit, PHPSpec, Behat)</li>
                            <li>MySQL profiling and query optimization</li>
                            <li>Creative and efficient problem solver</li>
                        </ul>
                        <h3>Benefits:</h3>
                        <ul class="list-group jd-ul-li">
                            <li>Opportunity to work with the best minds in the industry.</li>
                            <li>Great learning opportunity.</li>
                        </ul>
                        <label class="mt-4" style="color:#0D0D0D;">Know more about us <span style="color: #1660D6;">Ipat.tech</span></label>
                    </div>

                </div>

                    <div class="d-flex justify-content-between align-items-center next-prev-btn-div mr-0 ml-0">

                            <button type="button" class="d-flex justify-content-center align-items-center btn btn-outline-secondary prev-btn"><svg xmlns="http://www.w3.org/2000/svg" class="mr-1 h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18" />
                            </svg>
                            <span class="d-none d-sm-block">Previous</span></button>
                            
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="bookmrk-div-footer display-none mr-2"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8.684 13.342C8.886 12.938 9 12.482 9 12c0-.482-.114-.938-.316-1.342m0 2.684a3 3 0 110-2.684m0 2.684l6.632 3.316m-6.632-6l6.632-3.316m0 0a3 3 0 105.367-2.684 3 3 0 00-5.367 2.684zm0 9.316a3 3 0 105.368 2.684 3 3 0 00-5.368-2.684z" />
                                    </svg>
                                </div>
                                    
                                <button type="button" class="mt-0 btn btn-primary mb-0 common-btn footer-aply-btn mr-0">Apply</button>

                                <div class="bookmrk-div-footer display-none ml-2"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 bookmark-job" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                    </svg>
                                </div>
                            </div>

                            <button type="button" class="d-flex justify-content-center align-items-center btn btn-outline-secondary prev-btn">
                                <span class="d-none d-sm-block">Next</span>
                                <svg xmlns="http://www.w3.org/2000/svg" class="ml-1 h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3" />
                            </svg></button>
                    </div>  
                    
                    <div class="row">
                        <p class="secondary-font-style-small mt-5 mb-4">SImilar jobs</p>

                            <div class="col-lg-3 col-md-6 mt-2 mb-2">
                                <div class="common-data-div ">
                                    <div class="job-dsply-div mt-0 d-flex justify-content-between flex-lg-row flex-md-column ">
            
                                        <div class="bookmrk-div"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 bookmark-job" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                            </svg>
                                        </div>

                                        <div class="">
                                            <p class="secondary-font-style">PHP Developer</p>
                                            <h4 class="mb-3">IPAT Tech Solution</h4>
                                            <div class="d-flex align-items-center flex-wrap mb-3">

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">New york, US</p>
                                                </div>

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 14v3m4-3v3m4-3v3M3 21h18M3 10h18M3 7l9-4 9 4M4 10h16v11H4V10z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">Miles</p>
                                                </div>

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">2-5 Years</p>
                                                </div>

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">Remote</p>
                                                </div>

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 8h6m-5 0a3 3 0 110 6H9l3 3m-3-6h6m6 1a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">20k-60k</p>
                                                </div>

                                            </div>
                                            
                                            <p class="job-post-text-p " >Posted 5hrs ago</p>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 mt-2 mb-2">
                                <div class="common-data-div ">
                                    <div class="job-dsply-div mt-0 d-flex justify-content-between flex-lg-row flex-md-column ">
            
                                        <div class="bookmrk-div"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 bookmark-job" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                            </svg>
                                        </div>

                                        <div class="">
                                            <p class="secondary-font-style">PHP Developer</p>
                                            <h4 class="mb-3">IPAT Tech Solution</h4>
                                            <div class="d-flex align-items-center flex-wrap mb-3">

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">New york, US</p>
                                                </div>

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 14v3m4-3v3m4-3v3M3 21h18M3 10h18M3 7l9-4 9 4M4 10h16v11H4V10z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">Miles</p>
                                                </div>

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">2-5 Years</p>
                                                </div>

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">Remote</p>
                                                </div>

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 8h6m-5 0a3 3 0 110 6H9l3 3m-3-6h6m6 1a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">20k-60k</p>
                                                </div>

                                            </div>
                                            
                                            <p class="job-post-text-p " >Posted 5hrs ago</p>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 mt-2 mb-2">
                                <div class="common-data-div ">
                                    <div class="job-dsply-div mt-0 d-flex justify-content-between flex-lg-row flex-md-column ">
            
                                        <div class="bookmrk-div"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 bookmark-job" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                            </svg>
                                        </div>

                                        <div class="">
                                            <p class="secondary-font-style">PHP Developer</p>
                                            <h4 class="mb-3">IPAT Tech Solution</h4>
                                            <div class="d-flex align-items-center flex-wrap mb-3">

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">New york, US</p>
                                                </div>

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 14v3m4-3v3m4-3v3M3 21h18M3 10h18M3 7l9-4 9 4M4 10h16v11H4V10z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">Miles</p>
                                                </div>

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">2-5 Years</p>
                                                </div>

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">Remote</p>
                                                </div>

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 8h6m-5 0a3 3 0 110 6H9l3 3m-3-6h6m6 1a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">20k-60k</p>
                                                </div>

                                            </div>
                                            
                                            <p class="job-post-text-p " >Posted 5hrs ago</p>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 mt-2 mb-2">
                                <div class="common-data-div ">
                                    <div class="job-dsply-div mt-0 d-flex justify-content-between flex-lg-row flex-md-column ">
            
                                        <div class="bookmrk-div"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 bookmark-job" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                            </svg>
                                        </div>

                                        <div class="">
                                            <p class="secondary-font-style">PHP Developer</p>
                                            <h4 class="mb-3">IPAT Tech Solution</h4>
                                            <div class="d-flex align-items-center flex-wrap mb-3">

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">New york, US</p>
                                                </div>

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 14v3m4-3v3m4-3v3M3 21h18M3 10h18M3 7l9-4 9 4M4 10h16v11H4V10z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">Miles</p>
                                                </div>

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">2-5 Years</p>
                                                </div>

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">Remote</p>
                                                </div>

                                                <div class="d-flex align-items-center job-locations">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 8h6m-5 0a3 3 0 110 6H9l3 3m-3-6h6m6 1a9 9 0 11-18 0 9 9 0 0118 0z" />
                                                    </svg>
                                                    <p class="black-primary mb-0">20k-60k</p>
                                                </div>

                                            </div>
                                            
                                            <p class="job-post-text-p " >Posted 5hrs ago</p>
                                        </div>

                                    </div>
                                </div>
                            </div>

                    </div>


            </div>


            
        </div>
    </div>
</section>
<!-- Search Page Section  -->



<?php include("../components/footer.php") ?>





