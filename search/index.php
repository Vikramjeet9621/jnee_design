<?php include("../components/header_search.php") ?>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header" style="background:#ECF3FF;">
        <h5 class="modal-title" id="exampleModalLabel">Filters</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="col-lg-12 ">
                    <div class="job-alert-div outer-boundary">

                        <div class="sort-div-1">
                            <p class="secondary-font-style-small mb-3">Sort By</p>
                            <div class="d-flex justify-content-between mr-1">
                                <p class="secondary-font-style-small secondary-para-style-small" >Salary</p>
                                <input type="radio" class="">
                            </div>
                            <div class="d-flex justify-content-between mr-1">
                                <p class="secondary-font-style-small secondary-para-style-small" >Date Posted</p>
                                <input type="radio" class="">
                            </div>
                            <div class="d-flex justify-content-between mr-1">
                                <p class="secondary-font-style-small secondary-para-style-small" >Location</p>
                                <input type="radio" class="">
                            </div>
                        </div>

                        <div class="sort-div-2">
                            <p class="secondary-font-style-small mb-3">Filters</p>
                            <div class="d-flex justify-content-between mb-3">
                                <h6 class="mb-0 secondary-font-style-small secondary-para-style-small" >Salary bands</h6>
                                <div class="d-flex align-items-center" ><input type="radio" class=""> <p class="ml-1 mb-0 secondary-font-style-small secondary-para-style-small" >All<p></div>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >$20-30K</p>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >$30-60K</p>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >$60-100K</p>
                            </div>
                        </div>

                        <div class="sort-div-3">
                            <div class="d-flex justify-content-between mb-3">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small" >Experience Level</p>
                                <div class="d-flex align-items-center" ><input type="radio" class=""> <p class="ml-1 mb-0 secondary-font-style-small secondary-para-style-small" >All<p></div>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >0-2 Years</p>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >2-5 Years</p>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >5+ Years</p>
                            </div>
                        </div>

                        <div class="sort-div-4">
                            <div class="d-flex justify-content-between mb-3">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small" >Location</p>
                                <div class="d-flex align-items-center" ><input type="radio" class=""> <p class="ml-1 mb-0 secondary-font-style-small secondary-para-style-small" >All<p></div>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >California</p>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >New York</p>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >Texas</p>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >Washington</p>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >Los Angeles</p>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >San Francisco</p>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <a href="#" class="blue-primary"><p class="blue-primary mb-0 secondary-font-style-small secondary-para-style-small-xx" >Show All</p></a>
                            </div>
                        </div>

                        <div class="sort-div-5">
                            <div class="d-flex justify-content-between mb-3">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small" >Position</p>
                                <div class="d-flex align-items-center" ><input type="radio" class=""> <p class="ml-1 mb-0 secondary-font-style-small secondary-para-style-small" >All<p></div>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >Lead Developer</p>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >Software</p>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >Senior Manager</p>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >Sr. Software Engineer</p>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >UI/UX Designer</p>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <input type="radio" class="mr-1">
                                <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >Product Designer</p>
                            </div>
                            <div class="d-flex justify-content-start align-items-center mb-3">
                                <a href="#" class="blue-primary"><p class="blue-primary mb-0 secondary-font-style-small secondary-para-style-small-xx" >Show All</p></a>
                            </div>
                        </div>

                    </div>
        </div>
      </div>
      <div class="modal-footer justify-content-between" style="background:#ECF3FF;">
        <button type="button" class="btn btn-secondary prev-btn" style="width:150px;" data-bs-dismiss="modal">Reset</button>
        <button type="button" class="btn btn-primary common-btn m-0">Apply Filters</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->

<!-- Header Code Ending -->

<!-- Search Page Section -->
<section class="search-page-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 main-job-alrt-container">
                <div class="job-alert-div mb-3">
                    <div class="d-flex align-items-center mail-head-div mb-3" >
                        <img src="../assets/images/mail.png" class="mr-1" />
                        <p class="secondary-heading-style">Create job alert</p>
                    </div>
                    <p class="secondary-font-style-small mb-3">Enter your Email below to receive your top new job matches directly in your inbox.</p>
                    <div>
                        <input type="email" placeholder="Enter your mail" class="form-control seacrh-jobs-input job-input-alert outer-boundary" >
                    </div>
                    <div class="find-jobs-btn-div">
                        <button type="button" class="btn btn-primary w-100 common-btn" style="border-radius: 8px;">Subscribe</button>
                    </div>
                </div>

                <div class="job-alert-div sorting-jobs-div">

                    <div class="sort-div-1">
                        <p class="secondary-font-style-small mb-3">Sort By</p>
                        <div class="d-flex justify-content-between mr-1">
                            <p class="secondary-font-style-small secondary-para-style-small" >Salary</p>
                            <input type="radio" class="">
                        </div>
                        <div class="d-flex justify-content-between mr-1">
                            <p class="secondary-font-style-small secondary-para-style-small" >Date Posted</p>
                            <input type="radio" class="">
                        </div>
                        <div class="d-flex justify-content-between mr-1">
                            <p class="secondary-font-style-small secondary-para-style-small" >Location</p>
                            <input type="radio" class="">
                        </div>
                    </div>

                    <div class="sort-div-2">
                        <p class="secondary-font-style-small mb-3">Filters</p>
                        <div class="d-flex justify-content-between mb-3">
                            <h6 class="mb-0 secondary-font-style-small secondary-para-style-small" >Salary bands</h6>
                            <div class="d-flex align-items-center" ><input type="checkbox" class=""> <p class="ml-1 mb-0 secondary-font-style-small secondary-para-style-small" >All<p></div>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >$20-30K</p>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >$30-60K</p>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >$60-100K</p>
                        </div>
                    </div>

                    <div class="sort-div-3">
                        <div class="d-flex justify-content-between mb-3">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small" >Experience Level</p>
                            <div class="d-flex align-items-center" ><input type="radio" class=""> <p class="ml-1 mb-0 secondary-font-style-small secondary-para-style-small" >All<p></div>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >0-2 Years</p>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >2-5 Years</p>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >5+ Years</p>
                        </div>
                    </div>

                    <div class="sort-div-4">
                        <div class="d-flex justify-content-between mb-3">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small" >Location</p>
                            <div class="d-flex align-items-center" ><input type="radio" class=""> <p class="ml-1 mb-0 secondary-font-style-small secondary-para-style-small" >All<p></div>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >California</p>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >New York</p>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >Texas</p>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >Washington</p>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >Los Angeles</p>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >San Francisco</p>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <a href="#" class="blue-primary"><p class="blue-primary mb-0 secondary-font-style-small secondary-para-style-small-xx" >Show All</p></a>
                        </div>
                    </div>

                    <div class="sort-div-5">
                        <div class="d-flex justify-content-between mb-3">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small" >Position</p>
                            <div class="d-flex align-items-center" ><input type="radio" class=""> <p class="ml-1 mb-0 secondary-font-style-small secondary-para-style-small" >All<p></div>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >Lead Developer</p>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >Software</p>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >Senior Manager</p>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >Sr. Software Engineer</p>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >UI/UX Designer</p>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <input type="radio" class="mr-1">
                            <p class="mb-0 secondary-font-style-small secondary-para-style-small-xx" >Product Designer</p>
                        </div>
                        <div class="d-flex justify-content-start align-items-center mb-3">
                            <a href="#" class="blue-primary"><p class="blue-primary mb-0 secondary-font-style-small secondary-para-style-small-xx" >Show All</p></a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-lg-9 col-md-8 main-jobs-container">

                <div class="common-data-div">

                    <div class="d-flex justify-content-between align-items-center page-showing-div">
                        <div class="left-side-data">
                            <p>Php developer jobs in New york, US</p>
                            <h4>Showing: Page 1 of 1,105 jobs</h4>
                        </div>
                        <div class="right-side-data d-flex align-items-center">
                            <h4 class="mr-1 mb-0">Create alert of this job</h4>
                            <div class="form-check form-switch">
                                <input class="form-check-input" type="checkbox" id="job-alrt-input" />
                            </div>
                        </div>
                    </div>

                    <a href="../apply_job/index.php" style="text-decoration: none;"> <div class="job-dsply-div">
                        <div class="bookmrk-div"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 bookmark-job" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                            </svg>
                        </div>
                        <p class="secondary-font-style">PHP Developer</p>
                        <h4>IPAT Tech Solution</h4>
                        <div class="d-flex align-items-center flex-wrap mb-2">

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                                </svg>
                                <p class="black-primary mb-0">New york, US</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 14v3m4-3v3m4-3v3M3 21h18M3 10h18M3 7l9-4 9 4M4 10h16v11H4V10z" />
                                </svg>
                                <p class="black-primary mb-0">Miles</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                                <p class="black-primary mb-0">2-5 Years</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                                </svg>
                                <p class="black-primary mb-0">Remote</p>
                            </div>
                        </div>

                        <label>Job Description: This is a very good job that you should apply to immediately and as the first thing hereThis is a very good job that you should apply to immediately and as the first thing here.</label>
                        
                        <p class="job-post-text-p" >Posted 5hrs ago</p>

                    </div></a>

                    <div class="job-dsply-div">
                        <div class="bookmrk-div"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 bookmark-job" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                </svg>
                            </div>
                        <p class="secondary-font-style">PHP Developer</p>
                        <h4>IPAT Tech Solution</h4>
                        <div class="d-flex align-items-center flex-wrap mb-2">

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                                </svg>
                                <p class="black-primary mb-0">New york, US</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 14v3m4-3v3m4-3v3M3 21h18M3 10h18M3 7l9-4 9 4M4 10h16v11H4V10z" />
                                </svg>
                                <p class="black-primary mb-0">Miles</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                                <p class="black-primary mb-0">2-5 Years</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                                </svg>
                                <p class="black-primary mb-0">Remote</p>
                            </div>
                        </div>

                        <label>Job Description: This is a very good job that you should apply to immediately and as the first thing hereThis is a very good job that you should apply to immediately and as the first thing here.</label>
                        
                        <p class="job-post-text-p" >Posted 5hrs ago</p>

                    </div>

                    <div class="job-dsply-div">
                        <div class="bookmrk-div"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 bookmark-job" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                </svg>
                            </div>
                        <p class="secondary-font-style">PHP Developer</p>
                        <h4>IPAT Tech Solution</h4>
                        <div class="d-flex align-items-center flex-wrap mb-2">

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                                </svg>
                                <p class="black-primary mb-0">New york, US</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 14v3m4-3v3m4-3v3M3 21h18M3 10h18M3 7l9-4 9 4M4 10h16v11H4V10z" />
                                </svg>
                                <p class="black-primary mb-0">Miles</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                                <p class="black-primary mb-0">2-5 Years</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                                </svg>
                                <p class="black-primary mb-0">Remote</p>
                            </div>
                        </div>

                        <label>Job Description: This is a very good job that you should apply to immediately and as the first thing hereThis is a very good job that you should apply to immediately and as the first thing here.</label>
                        
                        <p class="job-post-text-p" >Posted 5hrs ago</p>

                    </div>

                    <div class="job-dsply-div">
                        <div class="bookmrk-div"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 bookmark-job" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                </svg>
                            </div>
                        <p class="secondary-font-style">PHP Developer</p>
                        <h4>IPAT Tech Solution</h4>
                        <div class="d-flex align-items-center flex-wrap mb-2">

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                                </svg>
                                <p class="black-primary mb-0">New york, US</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 14v3m4-3v3m4-3v3M3 21h18M3 10h18M3 7l9-4 9 4M4 10h16v11H4V10z" />
                                </svg>
                                <p class="black-primary mb-0">Miles</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                                <p class="black-primary mb-0">2-5 Years</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                                </svg>
                                <p class="black-primary mb-0">Remote</p>
                            </div>
                        </div>

                        <label>Job Description: This is a very good job that you should apply to immediately and as the first thing hereThis is a very good job that you should apply to immediately and as the first thing here.</label>
                        
                        <p class="job-post-text-p" >Posted 5hrs ago</p>

                    </div>

                    <div class="job-dsply-div">
                        <div class="bookmrk-div"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 bookmark-job" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                </svg>
                            </div>
                        <p class="secondary-font-style">PHP Developer</p>
                        <h4>IPAT Tech Solution</h4>
                        <div class="d-flex align-items-center flex-wrap mb-2">

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                                </svg>
                                <p class="black-primary mb-0">New york, US</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 14v3m4-3v3m4-3v3M3 21h18M3 10h18M3 7l9-4 9 4M4 10h16v11H4V10z" />
                                </svg>
                                <p class="black-primary mb-0">Miles</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                                <p class="black-primary mb-0">2-5 Years</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                                </svg>
                                <p class="black-primary mb-0">Remote</p>
                            </div>
                        </div>

                        <label>Job Description: This is a very good job that you should apply to immediately and as the first thing hereThis is a very good job that you should apply to immediately and as the first thing here.</label>
                        
                        <p class="job-post-text-p" >Posted 5hrs ago</p>

                    </div>

                    <div class="job-dsply-div">
                        <div class="bookmrk-div"><svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 bookmark-job" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z" />
                                </svg>
                            </div>
                        <p class="secondary-font-style">PHP Developer</p>
                        <h4>IPAT Tech Solution</h4>
                        <div class="d-flex align-items-center flex-wrap mb-2">

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z" />
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z" />
                                </svg>
                                <p class="black-primary mb-0">New york, US</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 14v3m4-3v3m4-3v3M3 21h18M3 10h18M3 7l9-4 9 4M4 10h16v11H4V10z" />
                                </svg>
                                <p class="black-primary mb-0">Miles</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z" />
                                </svg>
                                <p class="black-primary mb-0">2-5 Years</p>
                            </div>

                            <div class="d-flex align-items-center job-locations">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-6 mr-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
                                </svg>
                                <p class="black-primary mb-0">Remote</p>
                            </div>
                        </div>

                        <label>Job Description: This is a very good job that you should apply to immediately and as the first thing hereThis is a very good job that you should apply to immediately and as the first thing here.</label>
                        
                        <p class="job-post-text-p" >Posted 5hrs ago</p>

                    </div>                


                </div>

                <div class="d-flex justify-content-between align-items-center next-prev-btn-div">

                        <button type="button" class="d-flex justify-content-center align-items-center btn btn-outline-secondary prev-btn"><svg xmlns="http://www.w3.org/2000/svg" class="mr-1 h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18" />
                        </svg>Previous</button>

                        <button type="button" class="d-flex justify-content-center align-items-center btn btn-outline-secondary prev-btn">Next<svg xmlns="http://www.w3.org/2000/svg" class="ml-1 h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 5l7 7m0 0l-7 7m7-7H3" />
                        </svg></button>
                    </div>  

            </div>
            
        </div>
</div>
</section>

<div class="manual-alert ">
    <div class="manual-alert-icon-div">

    </div>
    <div class="manul-alrt-cntnt">
        <h3>Successfully updated</h3>
        <p>Your data is saved Successfully.</p>
    </div>
</div>
<!-- Search Page Section  -->



<?php include("../components/footer.php") ?>





